var express =require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

app.use(bodyParser.json());
//product object
Product = require('./models/product');
//bidding object
Bidding = require('./models/bidding');
//Connect to Mongoose DB MLAB
var mongodbUri ='mongodb://kallon2019:kallon2019@ds235053.mlab.com:35053/store';
mongoose.connect(mongodbUri);
//Connect to Mongoose DB Locally
//mongoose.connect('mongodb://localhost/store');
let db = mongoose.connection;
//Error report when connection to MongoDB not possible
db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});
//Report when connection is successfully made to MongoDB (MLAB)
db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ] on mlab.com');
});


//Displayed to user the url to use
app.get('/', function(reg, res){

    res.send('Please use /api/biddings or /api/products');
});
//Getting the data
app.get('/api/products', function(req, res){
	Product.getProducts(function(err, products){
		if(err){
            res.json({ message: 'Product NOT Found!', errmsg : err } );
		}
        res.send(JSON.stringify(products,null,5));
	});

});
//Getting a single data through the id
app.get('/api/products/:_id', function(req, res){
    Product.getProductById(req.params._id, function(err, product){
        if(err){
            res.json({ message: 'Product NOT Found!', errmsg : err } );
        }
        res.json(product);
    });

});

//Adding a document to the database
app.post('/api/products', function(req, res){
	var product = req.body;
	Product.addProduct(product, function(err, product){
		if(err){
            res.json({ message: 'Product NOT Added!', errmsg : err } );
		}
        res.json({ message: 'Product Successfully Added!', data: product });
	});

});
//Editing an existing document in the database through the id
app.put('/api/products/:_id', function(req, res){
	var id = req.params._id;
	var product = req.body;
	Product.updateProduct(id, product,{}, function(err, product){
		if(err){
            res.json({ message: 'Product NOT Updated!', errmsg : err } );
		}
        res.json({ message: 'Product Successfully Updated!', data: product });
	});

});

//deleting a document in the database through the id
app.delete('/api/products/:_id', function(req, res){
	var id = req.params._id;
	Product.removeProduct(id, function(err, product){
		if(err){
            res.json({ message: 'Product NOT Deleted!', errmsg : err } );
		}
        res.json({ message: 'Product Successfully Deleted!', data: product });
	});

});

 
app.get('/api/biddings', function(req, res){
	Bidding.getBiddings(function(err, biddings){
		if(err){
            res.json({ message: 'Bidding NOT Found!', errmsg : err } );
		}
        //res.json({ message: 'All Bidding Successfully Retried!', data: biddings });
        res.send(JSON.stringify(biddings,null,5));
	});

});

app.get('/api/biddings/:_id', function(req, res){
	Bidding.getBiddingById(req.params._id, function(err, bidding){
		if(err){

            res.json({ message: 'Bid NOT Found!', errmsg : err } );
		}
        res.json({ message: 'Bid Found!', data: bidding });
	});

});


app.post('/api/biddings', function(req, res){
	var bidding = req.body;
	Bidding.addBidding(bidding, function(err, bidding){
		if(err){
            res.json({ message: 'Bidding NOT Added!', errmsg : err } );
		}
        res.json({ message: 'Bidding Successfully Added!', data: bidding });
	});

});


app.put('/api/biddings/:_id', function(req, res){
	var id = req.params._id;
	var bidding = req.body;
	Bidding.updateBidding(id, bidding, {}, function(err, bidding){
		if(err){
            res.json({ message: 'Bid NOT Updated!', errmsg : err } );
		}
        res.json({ message: 'Bid Successfully Updated!', data: bidding});
	});

});

app.delete('/api/biddings/:_id', function(req, res){
	var id = req.params._id;
	Bidding.removeBidding(id, function(err, bidding){
		if(err){
            res.json({ message: 'Bid NOT Deleted!', errmsg : err } );
		}
        res.json({ message: 'Bid Successfully Deleted!', data: bidding });
	});

});




app.listen(3000);
console.log('Runing on port 3000')