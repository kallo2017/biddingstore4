var mongoose = require('mongoose');

//Product Schema
var productSchema = mongoose.Schema({

    name:{
    	type: String,
    	required: true,
    },

     create_date:{
       type: Date, 
       default: Date.now
     }

});

var Product = module.exports = mongoose.model('Product', productSchema);

//Get Products

module.exports.getProducts = function(callback,limit){
 Product.find(callback).limit(limit);
}

//Get a product by id
module.exports.getProductById = function(id, callback){
 Product.findById(id, callback);
}
//Add Product
module.exports.addProduct = function(product, callback){
 Product.create(product, callback);
}
//Update Products
module.exports.updateProduct = function(id, product, options, callback){
 var query = {_id: id};
 var update = {
 	 name: product.name
 }
 Product.findOneAndUpdate(query, update, options, callback);
}

//Delete Product
module.exports.removeProduct = function(id, callback){
 var query = {_id: id}
 Product.remove(query, callback);
}