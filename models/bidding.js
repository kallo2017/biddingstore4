var mongoose = require('mongoose');

//Product Schema
var biddingSchema = mongoose.Schema({

    name:{
    	type: String,
    	required: true
    },
    location:{ 
    	type: String
    
    },
    biddproduct:{
    	type: String,
    	required: true
    },
    	
    offer:{
        type: Number,
        required: true
    },
    	


     create_date:{
       type: Date, 
       default: Date.now
     }

});

var Bidding = module.exports = mongoose.model('Bidding', biddingSchema);

//Get biddings
//As requested in your lectures i will be adding a call back here
module.exports.getBiddings = function(callback,limit){
 Bidding.find(callback).limit(limit);
}
//Get a bidds
module.exports.getBiddingById = function(id, callback){
 Bidding.findById(id, callback);
}

//Add a bidds
module.exports.addBidding = function(bidding, callback){
 Bidding.create(bidding, callback);
}
//Update Biddings
module.exports.updateBidding = function(id, bidding, options, callback){
 var query = {_id: id};
 var update = {
 	 name: bidding.name,
 	 location: bidding.location,
 	 biddproduct: bidding.biddproduct,
 	 offer: bidding.offer
 }
 Bidding.findOneAndUpdate(query, update, options, callback);
}
//Delete a bidd
module.exports.removeBidding = function(id, callback){
 var query = {_id: id}
 Bidding.remove(query, callback);
}