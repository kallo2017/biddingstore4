var app = require('../app.js');
var request = require('supertest')(app);

describe('Test for biddings route', function(){

    it('Test GET method for /api/bidding', function(done){
        request
            .get('/api/biddings')
            .expect(201)
            .expect('Content-Type', 'application/json; charset=utf-8')
            .end(function(err, res){
                if(err){
                    return done(err);
                }
                done();
            });
    });


    it('Test POST method for /api/biddings', function(done){
        var bidding = {name: 'kallon webapp', location: 'America', biddproduct: 'Washing Machine','offer':80};
        request
            .post('api/biddings')
            .send(bidding)
            .expect(200)
            .end(function(err, res){
                if(err){
                    return done(err);
                }
                bidding = res.body;
                done();
            });
    });

    it('Test PUT method for /api/biddings/:bidding_id', function(done){
        var biddingUpdate = {'name': 'kallon webapp', 'location': 'America', 'biddproduct': 'Washing Machine','offer':100};
        request
            .put('api/biddings/' + biddings._id)
            .send(biddingUpdate)
            .expect(200)
            .done();
    });
});



it('Test PUT method for /api/book/:book_id', function(done){
    var biddingDelete = {'name': 'kallon webapp', 'location': 'America', 'biddproduct': 'Washing Machine','offer':100};
    request
        .put('api/biddings/' + biddings._id)
        .send(bookEdit)
        .expect(200)
        .done();
});

it('Test DELETE method for /api/biddings/:bidding_id', function(done){
    var biddingDelete = {'name': 'kallon webapp', 'location': 'America', 'biddproduct': 'Washing Machine','offer':100};
    request
        .put('api/biddings/' + bidding._id)
        .send(biddingDelete)
        .expect(200)
        .done();
});


//reference  https://www.youtube.com/watch?v=r8sPUw4uxAI
